---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Declaring app data
@snapend

*src/main.js*

```js
import { createApp } from "vue";

const app = createApp({
  data: () => ({
    modalOpen: false
  })
});

app.mount("#app");
```

@[3-7]

Note:

A modal window can be in one of two states - opened, or closed. 

#### 3-7

Let's manage this state by adding a boolean data property `modalOpen` which we'll give an initial value of `false`.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Vue 2 way
@snapend

*src/main.js*

```js
const app = createApp({
  data: {
    modalOpen: false
  }
});
```

Note:

If we were to use Vue 2 syntax, we could do this by creating a `data` property on our app instance and assigning an object where our `modalOpen` property would be declared.

You can't do it this way anymore.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Vue 3 way
@snapend

*src/main.js*

```js
const app = createApp({
  data: () => ({
    modalOpen: false
  })
});
```

Note:

Now, the `data` config property must be assigned a *factory function which returns the state object*.

You'll probably be familiar with this because you always had to do this for Vue components, but now it's enforced for Vue app instances as well.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Vue 2 way
@snapend

*src/main.js*

```js
const app = createApp({
  data: {
    modalOpen: false
  }
});
```

Note:

While it does seem simpler to use an object rather than a factory function, it's not very beginner friendly to have two different ways of declaring component data.

It's better for beginners to avoid the mental load of having to remember and understand the difference.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Using a root component in Vue 3
@snapend

*src/main.js*

```js
import App from "./App.vue";

const app = createApp({
  ...
  render: h => h(App)
});

app.mount("#app");
```

@[1]
@[5]

Note:

The best practice for Vue projects is to put all root-level functionality into a root component rather than having it in the root instance.

#### 1

The typical way to do this is to create a component called `App`.

#### 5

If this were a Vue 2 project, you'd use a `render` function to render that component, so that you could avoid having the template compiler in your Vue build.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Using a root component in Vue 3
@snapend

*src/main.js*

```js
import App from "./App.vue";

const app = createApp(App);

app.mount("#app");
```

Note:

In Vue 3, there's a much simpler way. Now, you can simply pass a component to the `createApp` method and it will automatically make it your root component. 

This means you don't need to mess with a render function and you don't need the boilerplate config in your root instance.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Simplifying syntax
@snapend

*src/main.js*

```js
import App from "./App.vue";

createApp(App).mount("#app");
```

Note:

Now that we've done that, we can now simply the syntax by chaining the `mount` method staight on to the `createApp` method.

I think that's a elegant way to set up the Vue instance.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Defining root component
@snapend

*src/App.vue*

```html
<template></template>
<script>
export default {
  data: () => ({
    modalOpen: true  
  })
};
</script>
```

Note:

Let's move over to our root component now, `App.vue`.  Note that I've moved the logic I'd previously added to the root instance into the root component. 

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Toggling modal state with button
@snapend

*src/App.vue*

```html
<template>
  <button @click="toggleModalState">Open modal</button>
</template>
<script>
export default {
  data: () => ({ ... }),
  methods: {
   toggleModalState() { this.modalOpen = !this.modalOpen; }
  }
};
</script>
```

@[2,4,7-9]

Note:

Still in our root component,

#### 2,4,7-9

Let's now add a button and a click handling method to toggle the `modalOpen` state.

This is exactly as we'd do it in Vue 2, I only mention it so that you can follow the next steps.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Creating modal component
@snapend

*terminal*

```bash
$ touch src/components/Modal.vue
```

Note:

Going back to the terminal, let's now create our modal by adding a new component file called `Modal.vue`.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Creating modal component
@snapend

*src/components/Modal.vue*

```html
<template>
  <div class="modal">
    <slot></slot>
  </div>
</template>
```

Note:

For now, we'll just provide a minimal template for this component, including a slot for content.

We'll add more to this shortly.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Creating modal component
@snapend

*src/App.vue*

```html
<template>
  <button @click="toggleModalState">Open modal</button>
  <modal v-if="modalOpen">
    <p>Hello, I'm a Vue 3 modal window.</p>
  </modal>
</template>
<script>
...
</script>
```

@[3-5]
@[1-6]

Note:

Returning to the root component...

#### 3-5

Let's declare our modal component here. We'll render it conditional on the value of `modalOpen`. Let's also insert a paragraph of text into the slot for content.

#### 1-6

Now, do you notice anything odd about this component's template?

Well, there are two root elements. In Vue 3, thanks to a feature called *fragments*, it is no longer compulsory to have a single root element in a template as it was in Vue 2.