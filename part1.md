---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[span-100]
# Test-driving Vue 3
@snapend

Note:

Hey everyone, my name is Anthony Gore, and today we're going to take Vue 3 for a test drive by using it to create a simple app.

The purpose of this is to see if we can get a feel for working with Vue 3 and see where it's different and where it's the same as Vue 2.

We'll also have some discussion about why certain things have been added or changed in the Vue 3 API in order to understand the rationale.

There are a lot of new features in Vue 3 so I won't be able to cover everything, of course, but I'll try to cover the main features you've probably heard about including fragments, teleport, the Composition API, as well as some more obscure but important changes or additions. 

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[west span-100]
## About me
#### Follow me @anthonygore most places.

@ul
- Creator of VueJsDevelopers.com 
- Blogger, online course instructor
- Author of *Full-Stack Vue.js 2 & Laravel 5*, Packt Publishing, 2017
- Vue Community partner
@ulend

@snapend

Note:

A few things about me before we get started.

#### 1

I'm the creator of [Vue.js Developers](https://vuejsdevelopers.com) which is a resource to help developers improve their skill and knowledge of Vue beyond what's in the docs.

#### 2

I also blog and teach online courses and you can find a lot of my stuff on VuejsDevelopers.com.

#### 3

I'm also an author, I've got a book out called Full-Stack Vue 2 and Laravel 5 which, given the topic of this talk probably needs to be updated soon. 

#### 4

And I'm also happy to be a Vue community partner.

Alright, let's get started!

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### What we'll build
@snapend

Note:

Over the next 20 mins or so, I'm going to show you how I built a reusable modal window feature with Vue 3.  

So let's see what it looks like.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### What we'll build
@snapend

@snap[midpoint span-100 text-center]
![](assets/img/modal.gif)
@snapend

Note:

We'll watch it one more time in case you missed it. [repeat slide]

So as you can see it's nothing too impressive, but this basic app allows me to showcase a number of Vue 3's features which is the whole point.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Installation with Vue CLI
@snapend

*terminal*

```bash
$ vue create vue3-experiment
```

Note:

To get us up and running quickly, we'll use Vue CLI version 4.5 to create a new Vue project which you can name `vue3-experiment`.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Installation with Vue CLI
@snapend

@snap[midpoint span-100 text-center]
![](assets/img/terminal.png)
@snapend

Note:

Note that the recent version 4.5 of Vue CLI gives you the option to select either Vue 2 or Vue 3. If you're using an older version you'll need to manually change the Vue dependency to version 3 using NPM.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Resetting boilerplate
@snapend

*terminal*

```bash
$ cd vue3-experiment
$ : > src/main.js
$ : > src/App.vue
```

Note:

If you've used Vue CLI before you'll know that we get a number of boilerplate files.

So in the terminal, let's change into the Vue CLI project directory.

The next two commands will clear the contents of the entry file and the main component file so that we can create our Vue 3 app from scratch.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Creating a Vue 3 app 
@snapend

*src/main.js*

```js
import { createApp } from "vue";

const app = createApp({
  // app config
});

app.mount("#app");
```

@[1]
@[3-5]
@[7]

Note:

So now we'll open the entry file, *main.js*, where we declare our Vue 3 app.

#### 1

And straight off the bat, you can see that this has changed signficantly from Vue 2. 

So rather than using the `Vue` constructor and the `new` operator we now need to import the `createApp` function.

#### 3-5

We then call this function, passing in the app configuration object and we can optionally assign the created app to a variable. 

#### 7

We can then call the `mount` method on the app and pass it a CSS selector indicating our mount element, similar to how we did with Vue 2.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Reason for change?
#### Avoid permanent mutation of global state
@snapend

*src/main.js*

```js
import Vue from "vue";

// Affects both instances
Vue.mixin({ ... });

const app1 = new Vue({ el: "#app-1" });
const app2 = new Vue({ el: "#app-2" });
```

@[1-4]
@[6-7]

Note:

So why the change from using the Vue constructor to this new `createApp` function?

With the Vue constructor pattern, any global configuration that we added, for example, plugins, mixins, prototype properties, and so on, would *permanently mutate global state*.

#### 1-4

For example, if we add a mixin to the Vue 2 constructor,

#### 5-6

this mixin would be added to all apps created from this constructor whether we like it or not.

This really shows up as an issue in unit testing, as it makes it tricky to ensure that each test is isolated from the last.

The new API doesn't get polluted by any global configuration.